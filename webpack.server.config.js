const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {  server: './server.ts' }, // take this file
  resolve: { extensions: ['.js', '.ts'] },
  target: 'node', // build it for nodejs
  // this makes sure we include node_modules and other 3rd party libraries
  mode: 'none',
  externals: [/(node_modules|main\..*\.js)/], // be aware of node modules and dependencies
  output: {
    path: path.join(__dirname, 'dist'), // output it here
    filename: '[name].js' // and keep original filename
  },
  module: { // add a module loader for extra features
    rules: [
      { test: /\.ts$/, loader: 'ts-loader' }  // add the installed ts-loader which converts TS to JS
    ]
  },
  plugins: [  // extra tools which run over the compiled code
    // Temporary Fix for issue: https://github.com/angular/angular/issues/11580
    // for "WARNING Critical dependency: the request of a dependency is an expression"
    new webpack.ContextReplacementPlugin(
      /(.+)?angular(\\|\/)core(.+)?/,
      path.join(__dirname, 'src'), // location of your src
      {} // a map of your routes
    ),
    new webpack.ContextReplacementPlugin(
      /(.+)?express(\\|\/)(.+)?/,
      path.join(__dirname, 'src'),
      {}
    )
  ]
}
