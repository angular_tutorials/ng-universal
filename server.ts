// These are important and needed before anything else
// Are needed by angular to be correctly set up
import 'zone.js/dist/zone-node';
import 'reflect-metadata';

import { renderModuleFactory } from '@angular/platform-server'; // allows to bootstrap the app on the server
import { enableProdMode } from '@angular/core';

import * as express from 'express';
import { join } from 'path';  // allows to join multiple path segments/file names into a long absoulte path on the OS
// fs=file system package from node, allows to work with files, is needed to load index.html to parse/pre-render
import { readFileSync } from 'fs';

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

// Express server
const app = express();

const PORT = process.env.PORT || 4000;
const DIST_FOLDER = join(process.cwd(), 'dist');

// Our index.html we'll use as our template
const template = readFileSync(join(DIST_FOLDER, 'browser', 'index.html')).toString();

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const { AppServerModuleNgFactory, LAZY_MODULE_MAP } = require('./dist/server/main');

// provides lazy loading
const { provideModuleMap } = require('@nguniversal/module-map-ngfactory-loader');

app.engine('html', (_, options, callback) => {
  renderModuleFactory(AppServerModuleNgFactory, {
    // Our index.html
    document: template,
    url: options.req.url,
    // DI so that we can get lazy-loading to work differently (since we need it to just instantly render it)
    extraProviders: [
      provideModuleMap(LAZY_MODULE_MAP)
    ]
  }).then(html => {
    callback(null, html);
  });
});

// registers as html view engine, if a html file is selected
// the corresponding engine is started
app.set('view engine', 'html');
// where to finde the dist views, registers it as a view folder
app.set('views', join(DIST_FOLDER, 'browser'));

// Server static files from /browser
// Always return all files in the folder statically, allows to download the files
app.get('*.*', express.static(join(DIST_FOLDER, 'browser')));

// All regular routes use the Universal engine
app.get('*', (req, res) => {
  res.render(join(DIST_FOLDER, 'browser', 'index.html'), { req });
});

// Start up the Node server
app.listen(PORT, () => {
  console.log(`Node server listening on http://localhost:${PORT}`);
});
